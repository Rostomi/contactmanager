import React, { Component } from "react";
// import PropTypes from "prop-types";

class Contact extends Component {
  state = {
    contactsData: [],
  };

  componentDidMount() {
    fetch(
      "https://api.airtable.com/v0/apppswG4c6zUOlEzf/contactsTable?api_key=keyqfZTDvNbAkHY5r"
    )
      .then((res) => res.json())
      .then((res) => {
        console.log(res.records);
        this.setState({ contactsData: res.records });
      })
      .catch((error) => console.log(error));
  }

  render() {
    return this.state.contactsData.map((items) => {
      return (
        <div key={items.id} className="card card-body mb-3">
          <h4>{items.fields.Name}</h4>
          <ul className="list-group">
            <li className="list-group-item">{items.fields.eMail}</li>
            <li className="list-group-item">{items.fields.Phone}</li>
          </ul>
        </div>
      );
    });
  }
}

// Contact.propTypes = {
//   Name: PropTypes.string.isRequired,
//   eMail: PropTypes.string.isRequired,
//   Phone: PropTypes.string.isRequired,
// };

export default Contact;
